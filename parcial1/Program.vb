Imports System

Module Program
    Sub Main()

        Dim encabezado As String
        Console.Clear()
        encabezado = "201404248"
        Console.WriteLine(encabezado)
        encabezado = "Jesus Benjamin Hernandez Diaz"
        Console.WriteLine(encabezado)

        Console.WriteLine("---------------------------------------------")
        Console.WriteLine("Primer Parcial Programacion de computadoras 2")
        Console.WriteLine("---------------------------------------------")


        Dim opcion As Integer
        Do
            Console.WriteLine("[0] Salir")
            Console.WriteLine("[1] Terminacion_0_1.")
            Console.WriteLine("[2] Terminacion_2_3.")
            Console.WriteLine("[3] Terminacion_4_5.")
            Console.WriteLine("[4] Terminacion_6_7.")
            Console.WriteLine("[5] Terminacion_8.")
            Console.WriteLine("[6] Terminacion_9.")
            Console.WriteLine("Ingrese una opci�n: ")
            opcion = Console.ReadLine()
            Console.WriteLine("")


            Select Case opcion
                Case 1
                    TablasMultiplicar()

                Case 2
                    ContarVocales()

                Case 3
                    LongitudPalabra()

                Case 4
                    NumerosPrimos()

                Case 5
                    CadenaAlReves()

                Case 6
                    EnlistarPrimos()


            End Select

        Loop While opcion <> 0 Or opcion >= 7


    End Sub
    Private Sub LongitudPalabra()
        Dim p1, p2, p3 As String
        Console.WriteLine("Ingrese la palabra 1")
        p1 = Console.ReadLine()
        Console.WriteLine("Ingrese la palabra 2")
        p2 = Console.ReadLine()
        Console.WriteLine("Ingrese la palabra 3")
        p3 = Console.ReadLine()

        If p1.Length > p2.Length And p1.Length > p3.Length Then
            Console.WriteLine(p1 & " es la palabra con mayor longitud")
        ElseIf p2.Length > p1.Length And p2.Length > p3.Length Then
            Console.WriteLine(p2 & " es la palabra con mayor longitud")
        ElseIf p3.Length > p1.Length And p3.Length > p2.Length Then
            Console.WriteLine(p3 & " es la palabra con mayor longitud")
        End If
    End Sub

    Private Sub TablasMultiplicar()



        Dim n As Integer 'el numero
        Dim i As Integer 'el contador
        Dim t As Integer 'Fin de la tabla
        Dim d As Integer 'Inicio de la tabla
        Dim multiplo As Integer 'para los multiplos

        'Ingresar valores para las variables estaticas
        Console.Write("Ingrese un numero para mostrar tabla: ")
        n = Console.ReadLine

        Console.Write("Ingrese inicio de la tabla: ")
        d = Console.ReadLine

        Console.Write("Ingrese fin de la tabla: ")
        t = Console.ReadLine

        'Con esta l�nea se env�a a consola un mensaje      
        Console.WriteLine("Tabla de Multiplicar del numero {0}", n)


        'Realizar los procesos
        For i = d To t Step 1

            multiplo = n * i

            'Mostrar resultados en consola
            Console.WriteLine("{0} x {1} = {2}", n, i, multiplo)

        Next
    End Sub

    Private Sub CadenaAlReves()
        Dim palabra, invertido As String
        invertido = ""
        Console.WriteLine("Ingrese palabra para devolver al rev�s")
        palabra = Console.ReadLine()

        For i = palabra.Length() - 1 To 0 Step -1
            invertido = invertido & palabra.Chars(i)
        Next
        Console.WriteLine(invertido)

    End Sub


    Private Sub ContarVocales()

        Dim vocales As String = "aeiou"
        Dim contador() As Integer = {0, 0, 0, 0, 0}
        Dim cadena As String

        Console.WriteLine("Ingrese una palabra")
        cadena = Console.ReadLine()

        For i As Integer = 0 To cadena.Length() - 1 Step 1
            For j As Integer = 0 To vocales.Length() - 1 Step 1
                If cadena.Chars(i) = vocales.Chars(j) Then
                    contador(j) = contador(j) + 1
                End If
            Next
        Next
        For x = 0 To vocales.Length - 1 Step 1
            Console.WriteLine("Letra: " & vocales.Chars(x) & " aparece " & contador(x) & " Veces")
        Next

    End Sub

    Private Sub NumerosPrimos()
        Dim n1 As Integer
        Dim n2 As Integer
        Dim n3 As Integer
        Dim n4 As Integer
        Dim n5 As Integer
        Dim n6 As Integer
        Dim n7 As Integer
        Dim n8 As Integer
        Dim n9 As Integer
        Dim n10 As Integer
        Dim p As Integer = 0
        Dim q As Integer = 0
        Dim r As Integer = 0
        Dim s As Integer = 0
        Dim t As Integer = 0
        Dim u As Integer = 0
        Dim v As Integer = 0
        Dim w As Integer = 0
        Dim x As Integer = 0
        Dim y As Integer = 0
        Console.WriteLine("Ingrese n�mero")
        n1 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n2 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n3 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n4 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n5 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n6 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n7 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n8 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n9 = Console.ReadLine()
        Console.WriteLine("Ingrese n�mero")
        n10 = Console.ReadLine()


        For i As Integer = 1 To n1 + 1
            If n1 Mod i = 0 Then
                p = p + 1
            End If
        Next i
        If p <> 2 Then
            Console.WriteLine("El primer n�mero no es primo")
        Else
            Console.WriteLine("El primer n�mero  es primo")
        End If
        For i As Integer = 1 To n2 + 1
            If n2 Mod i = 0 Then
                q = q + 1
            End If
        Next i
        If q <> 2 Then
            Console.WriteLine("El segundo n�mero no es primo")
        Else
            Console.WriteLine("El segundo n�mero  es primo")
        End If
        For i As Integer = 1 To n3 + 1
            If n3 Mod i = 0 Then
                r = r + 1
            End If
        Next i
        If r <> 2 Then
            Console.WriteLine("El tercer n�mero no es primo")
        Else
            Console.WriteLine("El tercer n�mero  es primo")
        End If
        For i As Integer = 1 To n4 + 1
            If n4 Mod i = 0 Then
                s = s + 1
            End If
        Next i
        If s <> 2 Then
            Console.WriteLine("El cuarto n�mero no es primo")
        Else
            Console.WriteLine("El cuarto n�mero  es primo")
        End If
        For i As Integer = 1 To n5 + 1
            If n5 Mod i = 0 Then
                t = t + 1
            End If
        Next i
        If t <> 2 Then
            Console.WriteLine("El quinto n�mero no es primo")
        Else
            Console.WriteLine("El quinto n�mero  es primo")
        End If
        For i As Integer = 1 To n6 + 1
            If n6 Mod i = 0 Then
                u = u + 1
            End If
        Next i
        If u <> 2 Then
            Console.WriteLine("El sexto n�mero no es primo")
        Else
            Console.WriteLine("El sexto n�mero  es primo")
        End If
        For i As Integer = 1 To n7 + 1
            If n7 Mod i = 0 Then
                v = v + 1
            End If
        Next i
        If v <> 2 Then
            Console.WriteLine("El septimo n�mero no es primo")
        Else
            Console.WriteLine("El septimo n�mero  es primo")
        End If
        For i As Integer = 1 To n8 + 1
            If n8 Mod i = 0 Then
                w = w + 1
            End If
        Next i
        If w <> 2 Then
            Console.WriteLine("El octavo n�mero no es primo")
        Else
            Console.WriteLine("El octavo n�mero  es primo")
        End If
        For i As Integer = 1 To n9 + 1
            If n9 Mod i = 0 Then
                x = x + 1
            End If
        Next i
        If x <> 2 Then
            Console.WriteLine("El noveno n�mero no es primo")
        Else
            Console.WriteLine("El noveno n�mero  es primo")
        End If
        For i As Integer = 1 To n10 + 1
            If n10 Mod i = 0 Then
                y = y + 1
            End If
        Next i
        If y <> 2 Then
            Console.WriteLine("El d�cimo n�mero no es primo")
        Else
            Console.WriteLine("El d�cimo n�mero  es primo")
        End If

    End Sub

    Private Sub EnlistarPrimos()
        Dim num As Integer
        Dim n As Integer = 4
        Dim cont As Integer = 2
        Dim i As Integer
        Dim cad As String = ""

        Console.WriteLine("Ingrese un n�mero: ")
        num = Console.ReadLine()
        If (num > 2) Then
            cad = "2-3"
            While (cont < num)
                i = 2
                While (i <= n)
                    If (i = n) Then
                        cad = cad + "-" + Trim(n)
                        cont = cont + 1
                    Else
                        If (n Mod i = 0) Then
                            i = n
                        End If

                    End If
                    i = i + 1
                End While
                n = n + 1
            End While
            Console.WriteLine(cad)
        Else
            If (num > 0) Then
                If (num = 1) Then
                    Console.WriteLine("es primo 2")
                Else
                    Console.WriteLine("es primo 2,3")
                End If
            Else
                Console.WriteLine("ingrese un numero positivo")
            End If
        End If
    End Sub
End Module
