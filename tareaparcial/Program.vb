Imports System.IO

Module Program
    Sub Main()
        Dim opcion As Char = ""
        Dim encabezado As String
        While opcion <> "6"
            Console.Clear()
            encabezado = "201404248"
            Console.WriteLine(encabezado)
            encabezado = "Jesus Benjamin Hernandez Diaz"
            Console.WriteLine(encabezado)

            Console.WriteLine("1. Tabla de multiplicar")
            Console.WriteLine("2. Divisores")
            Console.WriteLine("3. Vocales")
            Console.WriteLine("4. Letra elegida")
            Console.WriteLine("5. Grabar en archivo texto")
            Console.WriteLine("6. salir")
            Try
                'ingreso de la opcion
                Console.WriteLine("seleccione una opcion: ")
                opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case opcion
                    Case "1"
                        Dim n As Integer 'el numero
                        Dim i As Integer 'el contador
                        Dim t As Integer 'tama�o de la tabla
                        Dim multiplo As Integer 'para los multiplos

                        'Ingresar valores para las variables estaticas
                        Console.Write("Ingrese un numero para mostrar tabla: ")
                        n = Console.ReadLine

                        Console.Write("Ingrese tama�o de la tabla: ")
                        t = Console.ReadLine

                        'Con esta l�nea se env�a a consola un mensaje      
                        Console.WriteLine("Tabla de Multiplicar del numero {0}", n)

                        'Realizar los procesos
                        For i = 1 To t Step 1

                            multiplo = n * i

                            'Mostrar resultados en consola
                            Console.WriteLine("{0} x {1} = {2}", n, i, multiplo)

                        Next

                        Console.ReadKey()
                    Case "2"
                        Dim nu As Integer
                        Dim div As Integer

                        Console.Write("Ingrese un numero para mostrar sus divisores: ")
                        nu = Console.ReadLine

                        For div = 1 To nu
                            If (nu Mod div) = 0 Then
                                Console.WriteLine(div)


                            End If
                        Next
                        Console.ReadKey()


                    Case "3"
                        Dim salir As Char
                        Dim contador As Integer = 0
                        Dim frase As String

                        Console.Write("Escriba una frase: ")
                        frase = Console.ReadLine()

                        For Each c As Char In frase
                            Select Case c
                                Case "a"
                                    contador = contador + 1
                                Case "e"
                                    contador = contador + 1
                                Case "i"
                                    contador = contador + 1
                                Case "o"
                                    contador = contador + 1
                                Case "u"
                                    contador = contador + 1
                            End Select
                        Next
                        Console.Write(contador)
                        Console.Write(vbCrLf & "Desea salir?:  ")
                        salir = Console.ReadLine
                    Case "4"
                        Dim letra, salir As Char
                        Dim contador As Integer = 0
                        Dim frase As String

                        Console.Write("Escriba una frase: ")
                        frase = Console.ReadLine()
                        Console.Write("ingrese la letra a buscar: ")
                        letra = Console.ReadLine()

                        For Each c As Char In frase
                            If c = letra Then
                                contador = contador + 1
                            End If
                        Next
                        Console.Write(contador)
                        contador = Console.ReadLine()

                        Console.Write("Desea salir?:  ")
                        salir = Console.ReadLine
                    Case "5"
                        Dim ruta As String = "C:\Jesus_201404248\primerparcial.txt"
                        Dim archivo As StreamWriter
                        archivo = File.AppendText(ruta)
                        Console.WriteLine("Coloque el nombre del archivo")
                        Dim nombreArchivo As String = Console.ReadLine
                        Dim frase As String
                        Console.WriteLine("Ingrese una frase")
                        frase = Console.ReadLine
                        archivo.WriteLine(frase)
                        archivo.Flush()
                        archivo.Close()
                End Select
            Catch ex As Exception
                Console.WriteLine(ex.Message)


            End Try
            If opcion <> "6" Then Console.WriteLine("Oprima cualquier tecla para volver al menu e ingrese de nuevo la opcion")
            Console.ReadKey()

        End While
    End Sub
End Module
